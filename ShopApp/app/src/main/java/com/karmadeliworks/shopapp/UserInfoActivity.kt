package com.karmadeliworks.shopapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class UserInfoActivity : AppCompatActivity() {

    var username: EditText = findViewById<EditText>(R.id.et_username)
    var firstName: EditText = findViewById<EditText>(R.id.et_first_name)
    var lastName : EditText = findViewById<EditText>(R.id.et_last_name)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        findViewById<Button>(R.id.b_update_info).setOnClickListener {
            updateUserInfo()
        }

    }


    fun updateUserInfo(){
        val mAuth = FirebaseAuth.getInstance()
        val db = FirebaseDatabase.getInstance().reference.child("ShopApp/Users")
        val map = mutableMapOf<String,Any>(
            "firstName" to firstName.text.toString(),
            "lastName" to lastName.text.toString(),
            "username" to username.text.toString()
        )
        db.child(mAuth.currentUser!!.uid).updateChildren(map)

    }}
