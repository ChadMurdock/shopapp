package com.karmadeliworks.shopapp

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView



class CurrentListAdapter: RecyclerView.Adapter<CurrentListAdapter.CurrentListViewHolder>() {


    class CurrentListViewHolder(item: View): RecyclerView.ViewHolder(item){

        val textView = item.findViewById<TextView>(R.id.currentList_tv)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CurrentListViewHolder{
        var view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.current_list, viewGroup, false)
        return CurrentListViewHolder(view)

    }

    override fun getItemCount(): Int {
        println("GET ITEM COUNT")
        return currentListArr.size
    }

    override fun onBindViewHolder(holder: CurrentListViewHolder, position: Int) {
        holder.textView.text = currentListArr[position].name

        val context = holder.itemView.context

        holder.itemView.setOnClickListener {
            val intent = Intent(context, CurrentListDetailActivity::class.java)

            println("creatorUID " + currentListArr[position].creatorUID)
            intent.putExtra("creatorUID", currentListArr[position].creatorUID)
            intent.putExtra("listID", currentListArr[position].id)
            context.startActivity(intent)
        }


    }
}