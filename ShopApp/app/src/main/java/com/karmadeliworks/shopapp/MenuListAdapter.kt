package com.karmadeliworks.shopapp

import android.app.Activity
import android.content.Intent
import android.provider.MediaStore
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat.startActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MenuListAdapter: RecyclerView.Adapter<MenuListAdapter.MenuListViewHolder>() {


    class MenuListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val text = itemView.findViewById<TextView>(R.id.textView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): MenuListViewHolder {
        var view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.home_screen_item, viewGroup, false)
        return MenuListViewHolder(view)
    }

    override fun getItemCount(): Int {
       return MenuList.menuArr.size
    }

    override fun onBindViewHolder(viewHolder: MenuListViewHolder, position: Int) {
        viewHolder.text.text = MenuList.menuArr[position].string
        viewHolder.itemView.setOnClickListener {

            val context = viewHolder.itemView.context
            val intent = Intent(context, MenuList.menuArr[position].activity::class.java)
            startActivity(context, intent, null)

        }


    }


}