package com.karmadeliworks.shopapp

import android.os.Bundle

import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class CreateListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_list)

        val nameOfList = findViewById<EditText>(R.id.listNameEditText)
        val items = findViewById<EditText>(R.id.itemsEditText)
        val saveList = findViewById<Button>(R.id.saveListBtn)


        saveList.setOnClickListener {

            var creatorUID = unwrap(FirebaseAuth.getInstance().currentUser?.uid)
            val db = FirebaseDatabase.getInstance().reference.child("ShopApp/Users/$creatorUID/Lists")
            var listID = unwrap(db.push().key)


            val map = mutableMapOf<String, Any>()
            map["name"] = nameOfList.text.toString()
            map["items"] = items.text.toString()
            map["id"] = listID
            map["creatorUID"] = creatorUID

            db.child(listID).updateChildren(map)
            Toast.makeText(this, "list created in db", Toast.LENGTH_LONG).show()

        }
    }

    companion object{
         fun <T: String?>unwrap(value: T): String{
            var result = ""
            value.let{
                result = it as String
            }
            return result
        }
    }


}
