package com.karmadeliworks.shopapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class CurrentListDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_list_detail)

        val nameTextView = findViewById<TextView>(R.id.currentListDetailName)
        val itemsTextView = findViewById<TextView>(R.id.currentListDetailItems)

        val creatorUID = intent.getStringExtra("creatorUID")
        val listID = intent.getStringExtra("listID")

        val db = FirebaseDatabase.getInstance().reference.child("ShopApp/Users/$creatorUID/Lists/$listID")


        db.addValueEventListener( object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {

                  val value = snapshot.getValue(CurrentLists::class.java)
                  nameTextView.text = CreateListActivity.unwrap(value?.name)
                  itemsTextView.text = CreateListActivity.unwrap(value?.items)
            }

        })


    }
}
