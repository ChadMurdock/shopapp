package com.karmadeliworks.shopapp

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity

data class Navigation(val string: String, val activity: AppCompatActivity)