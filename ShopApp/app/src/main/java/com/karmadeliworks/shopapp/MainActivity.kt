package com.karmadeliworks.shopapp

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import java.util.*
import android.widget.Toast

import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import com.facebook.FacebookSdk
import com.facebook.FacebookSdk.sdkInitialize
import com.facebook.login.LoginManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase


class MainActivity : AppCompatActivity() {

    val callbackManager = CallbackManager.Factory.create()
    //private FirebaseAuth mAuth;
// ...
// Initialize Firebase Auth

    override fun onStart() {
        super.onStart()

        FacebookSdk.fullyInitialize()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val EMAIL = "email"
        val mAuth = FirebaseAuth.getInstance()

        val loginButton = findViewById<LoginButton>(R.id.login_button)
        loginButton.setPermissions("email", "public_profile")



        val button = findViewById<Button>(R.id.loginFB)
        button.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
            LoginManager.getInstance().registerCallback(callbackManager, object: FacebookCallback<LoginResult>{
                override fun onSuccess(result: LoginResult?) {
                    Log.d("MAINACTIVITY", "FB LOGIN SUCCESSFUL")

                    //val cred = FacebookAuthProvider.getCredential(result.accessToken.token)

                    val credential = FacebookAuthProvider.getCredential(result?.accessToken!!.token)
                    println(" $credential : CEDRES")
                    mAuth.signInWithCredential(credential).addOnCompleteListener {

                        if(it.isSuccessful) {
                            println("SUCCESS FIREBASE SIGN IN")
                            val intent = Intent(applicationContext, MainMenuActivity::class.java)
                            startActivity(intent)

                            val db = FirebaseDatabase.getInstance().reference.child("ShopApp/Users")
                            val map = mutableMapOf<String,Any>("uid" to mAuth.currentUser!!.uid)

                            db.child(mAuth.currentUser!!.uid).updateChildren(map)

                        }else{
                            println("FAILED TO GET FIREBASE")
                            println(it.exception?.localizedMessage)
                        }
                    }
                }

                override fun onCancel() {
                    println("ON CALCEL")
                }

                override fun onError(error: FacebookException?) {
                    println("ERROR: " + error?.localizedMessage)
                }


            })
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)

        println("CALLBACK MANAGER")

    }
}
