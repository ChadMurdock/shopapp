package com.karmadeliworks.shopapp;

public class User {
    String firstName;
    String lastName;
    String username;
    String uid;


    public String getUid() { return uid; }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }


    private User(){}

    public User(String firstName, String lastName, String username, String uid){
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.uid = uid;
    }


}
