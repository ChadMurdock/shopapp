package com.karmadeliworks.shopapp;

public class CurrentLists {

   String name ;
   String items ;
   String id;
    String creatorUID;

    private CurrentLists(){}

    public CurrentLists(String name, String items, String id, String creatorUID){
        this.id = id;
        this.name = name;
        this.items = items;
        this.creatorUID = creatorUID;

    }

    public String getName() {
        return name;
    }

    public String getItems() {
        return items;
    }

    public String getId() {
        return id;
    }

    public String getCreatorUID() {
        return creatorUID;
    }
}
