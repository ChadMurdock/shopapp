package com.karmadeliworks.shopapp

class MenuList {

    companion object{
        val menuArr = ArrayList<Navigation>()

        init {
            menuArr.add(Navigation("Create List", CreateListActivity()))
            menuArr.add(Navigation("Current List", CurrentListActivity()))
            menuArr.add(Navigation("Previous List", PreviousListActivity()))
            menuArr.add(Navigation("Consider Getting Items Page", ConsiderItemsActivity()))
            menuArr.add(Navigation("Order Now", OrderNowActivity()))
            menuArr.add(Navigation("Settings", SettingsActivity()))
            menuArr.add(Navigation("User Info", UserInfoActivity()))
        }
    }


}