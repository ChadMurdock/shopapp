package com.karmadeliworks.shopapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*




var currentListArr = ArrayList<CurrentLists>()

class CurrentListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_list)

        val recyclerView = findViewById<RecyclerView>(R.id.currentList_rv)
        val adapter = CurrentListAdapter()
        val layoutManager = LinearLayoutManager(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager

        //check db for lists
        val db = FirebaseDatabase.getInstance().reference.child("ShopApp/Users/${FirebaseAuth.getInstance().currentUser!!.uid}/Lists")
        db.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(snapShot: DataSnapshot) {
                println(snapShot)
                currentListArr.removeAll(currentListArr)
                for (child in snapShot.children) {
                    val value = child.getValue(CurrentLists::class.java)
                    value?.let { currentListArr.add(it) }
                }
                adapter.notifyDataSetChanged()

            }

        })

    }
}
